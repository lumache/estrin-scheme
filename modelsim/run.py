from vunit import VUnit
import os

hdl_path = '../hdl/'

##### ------------------------------------------
## Simulator configuration (please update path)
##### ------------------------------------------
os.environ["VUNIT_MODELSIM_PATH"] = "C:\\lscc\\radiant\\2022.1\\modeltech\\win32loem"

##### ------------------------------------------
## VUnit instance initialization and packages addition
##### ------------------------------------------
vu = VUnit.from_argv(compile_builtins=True)
vu.add_com()
vu.add_random()
vu.add_osvvm()

##### ------------------------------------------
## Libraries definition
##### ------------------------------------------

# ... precompiled library (please update path)
# vu.add_vhdl_builtins(external={'lib_name': ['path']})

# Design
lib = vu.add_library('lib_top_estrin_scheme')
lib.add_source_files('../hdl/lib_top_estrin_scheme/*.vhd')

# Estrin scheme agent
lib = vu.add_library('lib_agent_essc')
lib.add_source_files('../hdl/lib_tb_top_estrin_scheme/lib_agent_essc/*.vhd')

# Top level testbench
lib = vu.add_library('lib_tb_top_estrin_scheme')
lib.add_source_files('../hdl/lib_tb_top_estrin_scheme/*.vhd')

##### ------------------------------------------
## Main function execution
##### ------------------------------------------
vu.main()
