library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library lib_top_estrin_scheme;
use lib_top_estrin_scheme.pkg_estrin_scheme.all;

library lib_tb_top_estrin_scheme;
use lib_tb_top_estrin_scheme.pkg_tb_estrin_scheme.all;

library lib_agent_essc;
use lib_agent_essc.pkg_agent_essc.all;

entity tb_top_estrin_scheme is
   generic(runner_cfg : string);
end tb_top_estrin_scheme;

architecture tb of tb_top_estrin_scheme is

    signal s_clock         : std_logic := '0';
    signal s_reset_n       : std_logic := '1';
    signal s_wishbone      : wishbone_t;

begin

   -- Clock generation
   s_clock <= not s_clock after c_clk_period/2;

   ----------------------------------------------
   --- [Agent] define
   ----------------------------------------------
   i_agent_essc : entity lib_agent_essc.agent_essc(sim)
   generic map (c_clk_period)
   port map (
      wishbone          => s_wishbone
   );
   
   ----------------------------------------------
   --- [DUT] Estrin Scheme
   ----------------------------------------------
   i_estrin_scheme : entity lib_top_estrin_scheme.top_estrin_scheme(rtl)
   port map (
      clock             => s_clock,
      reset_n           => s_reset_n,
      wishbone          => s_wishbone
   );

   ----------------------------------------------
   --- [Test Cases] define
   ----------------------------------------------
   p_test_cases : process
   begin
      test_runner_setup(runner, runner_cfg);
   
      -- Reset generation
      wait for 100 ns;
      s_reset_n   <= '0';
      wait for 1000 ns;
      s_reset_n   <= '1';

      -- Test cases
      if run("test case : define") then
         -- show_all(log_define, display_handler);
         -- show(get_logger(chk_define),  display_handler, pass);

         subscriptions;
         fire_random_test_scenario(net);
      end if;

      test_runner_cleanup(runner);
   end process;
   
end tb;
