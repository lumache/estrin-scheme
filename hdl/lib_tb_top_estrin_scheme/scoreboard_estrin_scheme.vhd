library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library lib_tb_top_estrin_scheme;
use lib_tb_top_estrin_scheme.pkg_tb_estrin_scheme.all;

library lib_agent_essc;
use lib_agent_essc.pkg_agent_essc.all;

entity scoreboard_estrin_scheme is
   -- generic();
   -- port();
end scoreboard_estrin_scheme;
 
architecture sim of scoreboard_estrin_scheme is
begin

   ----------------------------------------------
   --- [Scoreboard] define
   ----------------------------------------------
   p_scrbd_define : process
      variable msg_exp, msg_res              : msg_t;
   begin
      -- receive(net, scr_define, msg_exp);
      -- receive(net, act_define, msg_res);

      -- define_exp := pop(msg_exp);
      -- define_res := pop(msg_res);

      -- check_equal(chk_define, define_res, define_exp, "[Scoreboard] define");
      
      -- delete(msg_exp);
      -- delete(msg_res);
      wait;          -- define / remove
   end process;
   
end sim;
