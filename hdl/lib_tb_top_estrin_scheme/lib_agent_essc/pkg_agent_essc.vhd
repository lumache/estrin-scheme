library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

package pkg_agent_essc is

   ----------------------------------------------
   --- Actors
   ----------------------------------------------
   constant act_driver     : actor_t   := new_actor("driver");
   constant act_monitor    : actor_t   := new_actor("monitor");

   ----------------------------------------------
   --- Message types -- define
   ----------------------------------------------
   -- for the sequencer
   -- for the driver
   -- for the monitor

   ----------------------------------------------
   --- Loggers
   ----------------------------------------------
   constant log_driver     : logger_t  := get_logger("agent:driver");
   constant log_monitor    : logger_t  := get_logger("agent:monitor");
   
   ----------------------------------------------
   --- Test Scenarios
   ----------------------------------------------
   procedure fire_random_test_scenario (
      signal net     : inout network_t;
      transactions   : positive           := 10;
      test_duration  : time               := 10 us
   );
   
   ----------------------------------------------
   --- Practical conversion functions
   ----------------------------------------------
   -- define
   
end package pkg_agent_essc;

package body pkg_agent_essc is

   ----------------------------------------------
   --- Test Scenarios
   ----------------------------------------------
   procedure fire_random_test_scenario (
      signal net     : inout network_t;
      transactions   : positive           := 10;      -- define
      test_duration  : time               := 10 us    -- define
   ) is
      variable send_msg : msg_t;
   begin
         -- set_stop_level(error);
         
         -- define reset here
         
         -- perform transactions
         -- send_msg := new_msg;
         -- push(send_msg, transactions);
         -- send(net, act_sequencer, send_msg);
         
         wait for test_duration;    -- define : duration independency
         info("Testbench finished. Amount of transactions made : " & to_string(transactions));
   end;

   ----------------------------------------------
   --- Practical conversion functions
   ----------------------------------------------
   -- define
   
end package body pkg_agent_essc;
