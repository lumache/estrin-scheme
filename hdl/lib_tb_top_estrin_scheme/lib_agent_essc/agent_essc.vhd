library ieee;
use ieee.std_logic_1164.all;

library lib_top_estrin_scheme;
use lib_top_estrin_scheme.pkg_estrin_scheme.all;

library lib_agent_essc;

entity agent_essc is
   generic(CLK_PERIOD   : time);
   port(
      wishbone    :  inout wishbone_t
   );
end agent_essc;

architecture sim of agent_essc is
begin

   ----------------------------------------------
   --- [Sequencer] define
   ----------------------------------------------
   i_sequencer_essc : entity lib_agent_essc.sequencer_essc(sim)
   generic map(CLK_PERIOD);
   -- port map ();
   
   ----------------------------------------------
   --- [Driver] define
   ----------------------------------------------
   i_driver_essc : entity lib_agent_essc.driver_essc(sim)
   -- generic map()
   port map (
      wishbone          => wishbone
   );
   
   ----------------------------------------------
   --- [Monitor] define
   ----------------------------------------------
   i_monitor_essc : entity lib_agent_essc.monitor_essc(sim);
   -- generic map()
   -- port map ();

end sim;
