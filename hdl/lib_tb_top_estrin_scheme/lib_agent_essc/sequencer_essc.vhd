library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library lib_top_estrin_scheme;
use lib_top_estrin_scheme.pkg_estrin_scheme.all;

library lib_agent_essc;
use lib_agent_essc.pkg_agent_essc.all;

entity sequencer_essc is
   generic(CLK_PERIOD   : time);
   -- port();
end sequencer_essc;

architecture sim of sequencer_essc is
begin

   p_sequencer_essc : process
      variable recv_msg    : msg_t;
   begin
      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- define dispatch
      -- send to act_driver

      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- trace(log_sequencer, "[sequencer] define" & data);
      
      ----------------------------------------------------------
      -- delete(recv_msg);
      wait;          -- define / remove
      
   end process;
   
end sim;
