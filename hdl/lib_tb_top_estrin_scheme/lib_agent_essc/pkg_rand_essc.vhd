library ieee;
use ieee.std_logic_1164.all;

library osvvm;
use osvvm.RandomPkg.all;

package pkg_rand_essc is

   shared variable r : RandomPType;

   ----------------------------------------------
   --- Randomization functions -- define
   ----------------------------------------------
   -- impure function foo (bar : zee_t) return zee_t;
   
end package pkg_rand_essc;

package body pkg_rand_essc is

   ----------------------------------------------
   --- Randomization functions -- define
   ----------------------------------------------
   -- impure function foo (bar : zee_t) return zee_t;
   
end package body pkg_rand_essc;
