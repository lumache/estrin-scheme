library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library lib_top_estrin_scheme;
use lib_top_estrin_scheme.pkg_estrin_scheme.all;

library lib_agent_essc;
use lib_agent_essc.pkg_agent_essc.all;

entity driver_essc is
   -- generic();
   port(
      wishbone    :  inout wishbone_t
   );
end driver_essc;

architecture sim of driver_essc is
begin

   p_driver_essc : process
      variable recv_msg    : msg_t;
   begin
      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- receive(net, act_driver, recv_msg);

      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- define interface
      
      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- publish(net, act_driver, pub_msg);
      -- trace(log_driver, "[driver] define" & data);

      ----------------------------------------------------------
      -- delete(recv_msg);
      wait;          -- define / remove
      
   end process;
   
end sim;
