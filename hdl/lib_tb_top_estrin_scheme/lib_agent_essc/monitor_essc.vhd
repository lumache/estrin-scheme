library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library lib_top_estrin_scheme;
use lib_top_estrin_scheme.pkg_estrin_scheme.all;

library lib_agent_essc;
use lib_agent_essc.pkg_agent_essc.all;

entity monitor_essc is
   -- generic();
   -- port();
end monitor_essc;

architecture sim of monitor_essc is
begin

   p_monitor_essc : process
      variable pub_msg     : msg_t;
   begin
      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- define interface

      ----------------------------------------------
      --- define
      ----------------------------------------------
      -- publish(net, act_monitor, pub_msg);
      -- trace(log_monitor, "[monitor] define" & data);
      wait;          -- define / remove
   
   end process;

end sim;
