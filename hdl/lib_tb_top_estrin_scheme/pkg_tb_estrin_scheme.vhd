library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library lib_agent_essc;
use lib_agent_essc.pkg_agent_essc.all;

package pkg_tb_estrin_scheme is

   ----------------------------------------------
   --- Testbench constants
   ----------------------------------------------
   constant c_clk_hz       : integer   := 10e6;    -- 10 [MHz]
   constant c_clk_period   : time      := 1 sec / c_clk_hz;

   ----------------------------------------------
   --- Actors
   ----------------------------------------------
   -- constant scr_define     : actor_t   := new_actor("define");
   
   ----------------------------------------------
   --- Checkers
   ----------------------------------------------
   -- constant chk_define     : checker_t := new_checker("define");
   
   ----------------------------------------------
   --- Subscriptions
   ----------------------------------------------
   procedure subscriptions;
   
end package pkg_tb_estrin_scheme;

package body pkg_tb_estrin_scheme is

   ----------------------------------------------
   --- Testbench constants
   ----------------------------------------------

   ----------------------------------------------
   --- Subscriptions
   ----------------------------------------------
   procedure subscriptions is
   begin
      -- define
      -- subscribe(scr_name, act_driver);
      -- subscribe(scr_name, act_monitor);
   end;
   
end package body pkg_tb_estrin_scheme;
