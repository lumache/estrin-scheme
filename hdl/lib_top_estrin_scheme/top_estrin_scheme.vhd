library ieee;
use ieee.std_logic_1164.all;

library lib_top_estrin_scheme;
use lib_top_estrin_scheme.pkg_estrin_scheme.all;

entity top_estrin_scheme is
   port (
      clock          : in    std_logic;
      reset_n        : in    std_logic;
      wishbone       : inout wishbone_t
   );
end top_estrin_scheme;

architecture rtl of top_estrin_scheme is
begin
   -- define
end rtl;
