library ieee;
use ieee.std_logic_1164.all;

package pkg_estrin_scheme is

   type wishbone_t is record
      -- define
      define : std_logic;
   end record wishbone_t;

end package pkg_estrin_scheme;

-- Package Body Section
package body pkg_estrin_scheme is
end package body pkg_estrin_scheme;
