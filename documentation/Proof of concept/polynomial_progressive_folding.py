
def basic_block(A, B, x):
    return A + B * x

def compute_polynomial(coeffs, x, M):
    layer_dict = {1: coeffs}  # Starting with layer 1 containing the coefficients
    
    if not coeffs:
        return 0
    
    def process_layer(layer_dict):
        print(f"Layer dictionary before processing: {layer_dict}")
        
        used_terms = 0
        for current_layer in sorted(layer_dict.keys(), reverse=True):  # Sorting layers in descending order
            terms_at_layer = layer_dict[current_layer]
            print(f"Processing Layer {current_layer} terms: {terms_at_layer}")
        
            new_terms = []
            for i in range(0, min(M * 2, len(terms_at_layer) - 1), 2):
                if used_terms < 3:
                    new_terms.append(basic_block(terms_at_layer[i], terms_at_layer[i + 1], x ** (2 ** (current_layer - 1))))
                    used_terms += 1
            
            # Update the current layer with the remaining terms that were not processed
            remaining_terms = terms_at_layer[len(new_terms) * 2:]
            if remaining_terms:
                layer_dict[current_layer] = remaining_terms  # The unprocessed terms left in the current layer
            else:
                del layer_dict[current_layer]
            
            # Add the new terms to the next layer only if new terms exist
            if new_terms:  # Only if new_terms is not empty
                next_layer = current_layer + 1
                if next_layer not in layer_dict:  # Only add the next layer if it's not already in the dictionary
                    layer_dict[next_layer] = new_terms
                else:
                    layer_dict[next_layer].extend(new_terms)

        # If there are still terms to process (new_terms > 1), recurse
        if len(layer_dict) == 1 and len(next(iter(layer_dict.values()))) == 1:
            # Return the results of the last processed layer
            return layer_dict.get(max(layer_dict.keys()), [])
        else:
            return process_layer(layer_dict)

    # Start processing layers recursively
    result = process_layer(layer_dict)
    
    # Return the final value, which should be a single term after all layers are processed
    return float(result[0]) if result else 0

coefficients = list(range(1, 2049))
x_value = 1.37
M = 23
result = compute_polynomial(coefficients, x_value, M)

def direct_polynomial_evaluation(coeffs, x):
    return sum(coeffs[i] * (x ** i) for i in range(len(coeffs)))

expected_result = direct_polynomial_evaluation(coefficients, x_value)

print("Polynomial result (optimized):", result)
print("Polynomial result (direct evaluation):", expected_result)
